require 'spec_helper'

describe 'checkin da box' do
  #Quick Challenge: Add a form to the index page in your
  #application that has a checkbox and a submit button.
  #Without adding a model, and depending only on session,
  #ensure that the checkbox above appears in the state that it
  # was last time the form was submitted. In other words, if you
  # submit the form with the box checked, the page reappears with the
  # box checked; if unchecked when submitted, then the it will appear
  # unchecked. Ensure that this is still true when you simply load the
  # page using the url, and don't submit the form.

  it 'retains a checked state across requests' do
    visit '/index'
    check 'Da Box'
    click_button 'Do It'

    expect(page).to have_checked_field('Da Box')

    visit '/index'
    expect(page).to have_checked_field('Da Box')
  end

  it 'unchecks a previously checked state and maintains that state' do
    visit '/index'
    check 'Da Box'
    click_button 'Do It'

    uncheck 'Da Box'
    click_button 'Do It'

    expect(page).to_not have_checked_field('Da Box')

    visit '/index'
    expect(page).to_not have_checked_field('Da Box')
  end
end
